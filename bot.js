const { Telegraf } = require('telegraf');
const { timetable } = require('./timetable')

const bot = new Telegraf('1681304098:AAGuj_LtEQcgs4Wa68MFB121Ed1Nbkin5rk')
bot.start((ctx) => {
    ctx.reply('Привет. Хочешь немного расписаний своих любимых автобусов? /y      /n')
});
const hour = () => new Date().getHours();
const day = () => new Date().getDay();
const currentTimeTable = (time) => {
        const tablehour = +(time.charAt(0)+time.charAt(1))
        return hour() ===  tablehour || hour+1 === tablehour
    }
const reduceToNearestTime = (busRoutes) => {
    return busRoutes.map(({schedule, name}) => {
        const localTimeTable = schedule.filter((time) => currentTimeTable(time))
        return {name, schedule: localTimeTable}
    })
}
const dayOfTheWeek = (ctx, tt, direction) => {
    const mode = (day() === 0 || day() === 6) ? 'weekend': 'workdays';
    reduceToNearestTime(tt[mode][direction]).forEach(({name, schedule}) => {
        ctx.reply(`${name}, ${schedule.flat()}`)
    });
}
bot.hears('/y', (ctx) => ctx.reply('Ок, куда? /t или /yar'))
bot.hears('/t', (ctx) => dayOfTheWeek(ctx, timetable, 't'))
bot.hears('/yar', (ctx) => dayOfTheWeek(ctx, timetable, 'yar'))

// bot.help((ctx) => ctx.reply('Send me a sticker'))
// bot.on('sticker', (ctx) => ctx.reply('👍'))
// bot.hears('hi', (ctx) => ctx.reply('Hey there'))
bot.launch()

